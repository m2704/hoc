
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals



import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Home from './Home';
// import Index from './components/Blog/Index';
// import Detail from './components/Blog/Detail';
// import Register from './components/Member/Register';
// import Login from './components/Member/Login';
// import Account from './components/Account/Index'
import {
  BrowserRouter as Router,
  Routes,
  Route,
  
} from "react-router-dom";
// import ProductDetail from './components/ProductDetail';
// import Cart from './components/Cart/Cart';
// import Context from './components/Context';
// import Wishlist from './components/Cart/WishList';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <div>
  <Router>
    <App>
      <Routes>
        <Route exact path='/' element = {<Home />}/>

      
      </Routes>
    </App>
  </Router>
</div>,
  document.getElementById('root')
);

reportWebVitals();