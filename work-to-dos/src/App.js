
import { Component } from 'react';
import './App.css';
import ToDo from './ToDos';

class App extends Component {
  constructor(props){
    super(props)
    this.state={
      list_item: [],
      item : {
        id:"",
        text:"",
        check: false
      }
    }
    this.handleInput = this.handleInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.deleteItem = this.deleteItem.bind(this)
    this.editItem=this.editItem.bind(this)
    this.checkItem=this.checkItem.bind(this)
  }
  handleInput(e){
    this.setState({
        item :{
          text: e.target.value,
          id : Date.now(),
          check: false
        }
    })
  }

  handleSubmit(e){
    e.preventDefault();
    let {item,list_item}=this.state
    if(item.text != ""){
      let listName = localStorage.getItem("list")
      if(listName){
        list_item = JSON.parse(listName)
      }
      this.setState({
        list_item : list_item,
        item : {
          id:"",
          text:"",
          check:""
        }
      })
      list_item.push(item)
      localStorage.setItem("list",JSON.stringify(list_item))
    }
  }
  deleteItem(id){
    let list_item = JSON.parse(localStorage.getItem("list"))
    let filterArray = list_item.filter(item => item.id!=id)
    this.setState({
      list_item: filterArray
    })
    localStorage.setItem("list",JSON.stringify(filterArray))
  }
  editItem(text,id){
    let list_item = JSON.parse(localStorage.getItem("list"))
    
    list_item.map((value, key)=> {
        if(id == value.id) {
          value.text = text
          value.check =false
        }
    })
    this.setState({
      list_item: list_item
    })
    localStorage.setItem("list",JSON.stringify(list_item))
  }
  checkItem(id){
    let list_item = JSON.parse(localStorage.getItem("list"))
    
    list_item.map((value, key)=> {
      console.log(id)
      console.log(value.id)
        if(id == value.id) {
          value.check = !value.check
          console.log(value.check)
        }
    })
    this.setState({
      list_item: list_item
    })
    localStorage.setItem("list",JSON.stringify(list_item))
    
  }
  render(){
    console.log(this.state.list_item)
    return (
      <div className="App">
        <h2 className="title">work to-dos</h2>
        <p className="sub-title-input">Enter text into the input field to add items to yours list</p>
        <p className="sub-title-mark">Click the item to mark it as complete</p>
        <p className="sub-title-remove">Click the "X" to remove the item from your list</p>
        <form className="add-field" onSubmit={this.handleSubmit}>
          <input type="text" className="add" placeholder="New item..." onChange={this.handleInput} value={this.state.item.text}></input>
          <button className="icon" type="submit" ><i class="fas fa-pencil-alt"></i></button>
        </form>
        <ul>
          <ToDo deleteItem={this.deleteItem} editItem={this.editItem} checkItem={this.checkItem}/>
        </ul>
      </div>
    );
  }
    

  
}

export default App;
