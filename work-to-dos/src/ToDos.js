import './App.css';
import { Component } from 'react';

class ToDo extends Component {
    constructor(props){
        super(props)
        this.getID = this.getID.bind(this)
        this.editValue = this.editValue.bind(this)
        this.check = this.check.bind(this)
    }

    getID(e){
        console.log(e.target.id)
        this.props.deleteItem(e.target.id)
    }
    editValue(e){
        this.props.editItem(e.target.value,e.target.id)

    }
    check(e){
        this.props.checkItem(e.target.id)
    }
    renderData(){
        let listItem= JSON.parse(localStorage.getItem("list")) 
        if(listItem){
            return listItem.map((value,key) => {
                return(
                <li className={value.check == true ? "item-check":"item"} id={value.id} onClick={this.check}>
                    <input id = {value.id} type="text" className="item-field" onChange={this.editValue} value={value.text}/>
                    
                    <a ><i id = {value.id} onClick = {this.getID} class="fa fa-remove"></i></a>
                </li>
                )
            })
        }
        
    }
    render(){
        return(
            <>
            {this.renderData()}
            </>
        )
        
    }
   

  
}

export default ToDo;
