import React from 'react';
import ReactDOM from 'react-dom';
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route
  
// } from "react-router-dom";
import './index.css';
import App from './App';
// import Home from './Home';
// import Account from './Account';
// import Login from './Login';
// import Content from './Content';



ReactDOM.render(
  // <div>
  //     <Router>
  //       <App>
  //         <Switch>
  //           <Route exact path='/' component = {Home}/>
  //           <Route path='/login' component = {Login}/>
  //           <Route path='/account' component = {Account}/>
  //         </Switch>
  //       </App>
  //     </Router>
  // </div>,
  <App/>,
  document.getElementById('root')
);
