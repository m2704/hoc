import {Component} from 'react';
const pStyle = {
    color: 'red'
  };
class FormErrors extends Component{
    renderError(){
        let formErrors = this.props.formErrors
        if(Object.keys(formErrors).length > 0){
            return Object.keys(formErrors).map((key, i)=>{
                return(
                    <p style={pStyle} key={i}>{formErrors[key]}</p>
                )
            })

        }
    }

    render(){
        
        return(
            <div className='formErrors'>
                {this.renderError()}
            </div>
        );
      
  }
}
export default FormErrors;