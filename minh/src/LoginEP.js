import React, {Component} from 'react';
import FormErrors from './FormErrors';
class LoginEP extends Component {
  constructor(props){
    super(props) 
    this.state = {
      email: '',
      pass:'',
      formErrors:{},
    //   emailError:'',
    //   passError:'',

    }
    // this.handleEmail= this.handleEmail.bind(this);
    // this.handlePass= this.handlePass.bind(this);
    this.handleInput = this.handleInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInput(e){
        const nameInput = e.target.name;
        const value = e.target.value;

        this.setState({
            [nameInput]: value
        })
    }
    // handleEmail(e){
    //     this.setState({
    //         email: e.target.value
    //     })
    // }
    // handlePass(e){
    //     this.setState({
    //         pass: e.target.value
    //     })
    // } 
    handleSubmit(e){
        e.preventDefault();

        let flag = true
        let email = this.state.email;
        let pass = this.state.pass;
        let errorSubmit = this.state.formErrors;

        if(!email){
            flag = false;
            errorSubmit.email = "Vui long nhap email";
            // this.setState({
            //     emailError: "vui long nhap email"
            // })
        }else {
            errorSubmit.email = "";
        }
        if(!pass){
            flag = false;
            errorSubmit.pass = "Vui long nhap password";
            // this.setState({
            //     passError: "vui long nhap password"
            // })
        }
        else {
            errorSubmit.pass = "";
        }

        if(!flag){
            this.setState({
                formErrors : errorSubmit
            })
            
        }
    }
    render() {
    
        return (
        <div className="App">
            <FormErrors formErrors={this.state.formErrors}/>
            <form onSubmit={this.handleSubmit}>
                    <input type="text" placeholder="Email" name="email" onChange={this.handleInput}/>
                    <br/>
                    <input type="pass" name="pass" onChange={this.handleInput}/>
                    <br/>
                    <span>
                        <input type="checkbox" className="checkbox"/>Keep me signed in
                    </span>
                    <br/>
                    <button type="submit" className="btn btn-default">Login</button>
            </form>
            
        </div>
        );
    }
}

export default LoginEP;