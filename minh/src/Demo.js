import React, {Component, useCallback} from 'react';
import { Link } from 'react-router-dom';
class Demo extends Component {
  constructor(props){
    super(props) 
    
    this.state = {
      demo: true,
      arr : [1,2,3,4,5,6]
        
    }
    this.thaydoi=this.thaydoi.bind(this)
  }
  thaydoi(){
      let {demo} = this.state;
      this.setState({
          demo : !demo
      })
  }
  showArray(){
      let {arr}=this.state
      if(arr.length > 0){
          return arr.map((value,key) =>{
              return(
                  <li key={key}>{value}</li>
              )
          })
      }
  }
  render() {
   let {demo} = this.state
    return (
      <div className="App">
          <button onClick={this.thaydoi}>Click</button>
          {demo==true ? "ON":"OFF"}
            <ul>
                {this.showArray()}
            </ul>
          
      </div>
    );
  }
}

export default Demo;