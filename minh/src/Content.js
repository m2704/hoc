import { Component } from 'react';

class Content extends Component{
  render(){
    return (
      <div className="content">
        <div className="con-right">
          <ul>
            <li>
              <img src="https://znews-photo.zadn.vn/w960/Uploaded/ovhpaob/2021_08_15/Long2_1.jpg" />
              <h2>Vì sao giãn cách xã hội nhưng số ca mắc Covid-19 vẫn tăng nhanh?</h2>
              <p>Theo Bộ trưởng Y tế, số ca mắc tăng rất nhanh cho thấy dịch đã lan rộng trong cộng đồng, trải qua nhiều vòng lây nhiễm mà chưa được phát hiện.</p>
              <span>Bộ Y tế lý giải mốc 15/9 kiểm soát dịch ở TP.HCM</span>
            </li>
            <li>
              <img src="https://znews-photo.zadn.vn/w480/Uploaded/zdhwqmjwq/2021_08_15/q.jpeg" />
              <p>Xả chốt chặn cầu Bình Triệu do ùn ứ khi khai báo di chuyển nội địa</p>
            </li>
            <li>
              <img src="https://znews-photo.zadn.vn/w480/Uploaded/ovhpaob/2021_08_15/ttxvn_thu_tuong_pham_minh_chinh_2.jpg" />
              <p>Thủ tướng đánh giá giãn cách xã hội chưa đạt mục tiêu đề ra</p>
            </li>
          </ul>
        </div>
        <div className="con-left">
          <ul>
            <li>
              <img src="https://znews-photo.zadn.vn/w210/Uploaded/aobohun/2021_08_15/Phong2.jpg" />
              <p>TP.HCM lên kế hoạch chống dịch ra sao trong một tháng tới?</p>
            </li>
            <li>
              <img src="https://znews-photo.zadn.vn/w210/Uploaded/mftsy/2021_08_12/2021_08_03T175339Z_782446421_RC2TXO92WWZM_RTRMADP_3_AFGHANISTAN_CONFLICT_BLAST_1_.jpg" />
              <p>Taliban bao vây thủ đô Kabul</p>
            </li>
            <li>
              <img src="https://znews-photo.zadn.vn/w210/Uploaded/kbd_ivdb/2021_07_17/phamngon6_1.jpg" />
              <p>Đồng Nai giãn cách xã hội đến hết ngày 31/8</p>
            </li>
            <li>
              <img src="https://znews-photo.zadn.vn/w210/Uploaded/qfssu/2021_08_14/40396308_2206343409587191_6484373071804235776_n_1.jpg" />
              <p>Gia đình á hậu có 7 người mắc Covid-19 ở TP.HCM</p>
            </li>
            <li>
              <img src="https://znews-photo.zadn.vn/w210/Uploaded/pwivovlb/2021_08_15/chungzing_1.jpg" />
              <p>Ông Nguyễn Đức Chung từng đe dọa Chánh thanh tra TP về vụ Redoxy 3C</p>
            </li>
          </ul>
        </div>
      </div>
    )
  }
}
export default Content;
