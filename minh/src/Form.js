import React, {Component} from 'react';
const pStyle = {
    background: 'red'
  };
class Form extends Component {
  constructor(props){
    super(props) 
    
    this.state = {
      email: "",
      pass:"",
      err_e:"",
      err_p:"",
      msg:""
    }
    
    // this.xulyEmail = this.xulyEmail.bind(this)
    // this.xulyPass = this.xulyPass.bind(this)
    this.xulyForm = this.xulyForm.bind(this)
    this.xulyInput = this.xulyInput.bind(this)


    // state: 
    //     - chi dc su dung trong 1 component  va de su dung state tu A->B 
    //     - dung de luu nhung gia tri lien tuc thay doi
    // props: 
    //     - chi dc su dung trong 1 component  va de su dung state tu A->B 
    //     - dung luu nhung gia tri co dinh
    
  }

 
//   xulyEmail(e){
//     //   cho phep minh moi thu trong the input attr
//     let get = e.target.value
//     this.setState({
//         // lay gia tri vao set vao bien email
//         email: get
//     })
//   }
//   xulyPass(e){
//     //   cho phep minh moi thu trong the input attr
//     let get = e.target.value
//     this.setState({
//         // lay gia tri vao set vao bien email
//         pass: get
//     })
//   }
    xulyInput(e){
        let value = e.target.value
        let name = e.target.name
        this.setState({
            // lay gia tri vao set vao bien email
            [name]: value
        })
    }




  xulyForm(e){
    e.preventDefault();

    let {email, pass} = this.state
    let demo = true;
    if(email == "") {
        this.setState({
            err_e:"vui long nhap e"
        })
        demo = false
    }else {
        this.setState({
            err_e: ""
        })
    }


    if(pass == "") {
        this.setState({
            err_p:"vui long nhap p"
        })
        demo = false
    }else {
        this.setState({
            err_p: ""
        })
    }   

    // if(email != "" && pass != "") {
    if(demo){
        this.setState({
            msg:"ok"
        })
     }

  }
 
  render() {
   
    return (
      <div className="App">
           <p style={pStyle}>{this.state.msg}</p>
          <form onSubmit={this.xulyForm}>

              <input name="email" value={this.state.email} onChange={this.xulyInput} />
              <p style={pStyle}>{this.state.err_e}</p>
              <input name="pass" value={this.state.pass} onChange={this.xulyInput} />
              <p style={pStyle}>{this.state.err_p}</p>

              <button type="submit">click</button>
          </form>
          
      </div>
    );
  }
}

export default Form;