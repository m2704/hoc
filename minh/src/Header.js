import { Component } from "react";
import { Link } from "react-router-dom";

class Header extends Component {
    render(){
        
        return (
            <div>
                <ul>
                    <li>
                        <Link to='/Home'>home</Link>
                    </li>
                    <li>
                        <Link to='/Account'>account</Link>
                    </li>
                    <li>
                        <Link to='/Login'>login</Link>
                    </li>
                </ul>
            </div>
            
        );
    }
    
  }
export default Header;