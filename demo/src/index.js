import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Home from './Home';
import Cart from './Cart';
import Checkout from './Checkout';
import {
  BrowserRouter as Router,
  Switch,
  Route
  
} from "react-router-dom";

ReactDOM.render(
  <div>
  <Router>
    <App>
      <Switch>
        <Route exact path='/' component = {Home}/>
        <Route path='/Cart' component = {Cart}/>
        <Route path='/Checkout' component = {Checkout}/>
      </Switch>
    </App>
  </Router>
</div>,
  document.getElementById('root')
);

