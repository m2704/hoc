import logo from './logo.svg';
import './App.css';
import {Component} from 'react';
import { withRouter } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';

class App extends Component{
  constructor(props){
      super(props)
  }
  render(){
      
      return(
            <>
                <Header />
                {this.props.children}
                <Footer/>
            </>
      )
      
  }

}
export default withRouter(App);
