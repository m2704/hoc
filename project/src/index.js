import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Home from './Home';
import Index from './components/Blog/Index';
import Detail from './components/Blog/Detail';
import Register from './components/Member/Register';
import Login from './components/Member/Login';
import Account from './components/Account/Index'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  
} from "react-router-dom";
import ProductDetail from './components/ProductDetail';
import Cart from './components/Cart/Cart';
import Context from './components/Context';
import Wishlist from './components/Cart/WishList';


ReactDOM.render(
  <div>
  <Router>
    <App>
      <Switch>
        <Route exact path='/' component = {Home}/>

        <Route  path='/register' component = {Register}/>
        <Route  path='/login' component = {Login}/>

        <Route  path='/product/detail/:id' component = {ProductDetail}/>
        <Route  path='/cart' component = {Cart}/>
        <Route  path='/wishlist' component = {Wishlist}/>
        <Route  path='/context' component = {Context}/>

        <Route  path='/blog/list' component = {Index}/>
        <Route  path='/blog/detail/:id' component = {Detail}/>
        <Route component = {Account}/>
      </Switch>
    </App>
  </Router>
</div>,
  document.getElementById('root')
);

