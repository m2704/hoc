import React, { Component } from "react";
const MyContext= React.createContext();
class MyProvider extends Component{
    state = {
        name: 'Dan',
        age: 100,
        cool: true
    }
    render(){
        return(
            <MyContext.Provider value={{
                state: this.state,
                growAYearOlder: ()=> this.setState({
                    age: this.state.age +1
                })
            }}>
                {this.props.children}
            </MyContext.Provider>
        )
    }
}
const Family = (props)=>(
    <div>
        <Person/>
    </div>
)
class Person extends Component{
    render(){
        return(
            <div>
                <MyContext.Consumer>
                    {(context) => (
                        <React.Fragment>
                            <p>{context.state.age}</p>
                            <p>{context.state.name}</p>
                            <button onClick={context.growAYearOlder}>Click</button>
                        </React.Fragment>
                    )}
                </MyContext.Consumer>
            </div>
        )
    }
}
class Context extends Component{
    render(){
        return(
            <MyProvider>
                <div>
                    ABC
                    <Family/>
                </div>
            </MyProvider>
        )
    }
    

}export default Context;