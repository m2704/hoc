
import axios from 'axios';
import React, {Component} from 'react';
import Errors from '../../Errors';
import Login from './Login';
class Register extends Component {
  constructor(props){
    super(props) 
    this.state = {
      name: '',
      email: '',
      password:'',
      phone:'',
      address:'',
      country:'',
      avatar: '',
      files:'',
      level:0,
      formErrors: {},

    }
    
    this.handleInput = this.handleInput.bind(this)
    this.handleInputFile = this.handleInputFile.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    
    }
    handleInput(e){
        const nameInput = e.target.name;
        const value = e.target.value;

        this.setState({
            [nameInput]: value
        })
    }
    handleInputFile(e){
        const file = e.target.files;
        //console.log(file[0])
        let reader = new FileReader()
        reader.onload = (e) => {
            this.setState({
            avatar: e.target.result,//gửi api qua
            files: file[0]
            })
        };
        reader.readAsDataURL(file[0])
        
    }
    handleSubmit(e){
        e.preventDefault();
        
        let flag = true
        let name = this.state.name;
        let email = this.state.email;
        let password = this.state.password;
        let phone = this.state.phone;
        let address = this.state.address;
        let country = this.state.country;
        let files = this.state.files;
        let errorSubmit = this.state.formErrors;

        let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        
        errorSubmit.phone = errorSubmit.name = errorSubmit.country = errorSubmit.address = errorSubmit.email = errorSubmit.password = errorSubmit.files = errorSubmit.avatar = ""

        if(name == ""){
            errorSubmit.name = "Vui long nhap name";
            flag = false;
        }

        if(email == ""){
            errorSubmit.email = "Vui long nhap email";
            flag = false;
        }else if( !emailReg.test(email) ) {
                flag = false;
                errorSubmit.email = "Vui long nhap dung dinh dang email ";
            }

        if(password == ""){
            errorSubmit.password = "Vui long nhap password";
            flag = false;
        }

        if(phone == ""){
            errorSubmit.phone = "Vui long nhap phone";
            flag = false;
        }else if( !filter.test(phone) ) {
            flag = false;
            errorSubmit.phone = "Vui long nhap dung dinh dang phone ";
        }

        if(address == ""){
            errorSubmit.address = "Vui long nhap address";
            flag = false;
        }

        if(country == ""){
            errorSubmit.country = "Vui long nhap country";
            flag = false;
        }

        if(files == ""){
            errorSubmit.files = "Vui long chon file";
            flag = false;
        }else {
            let checkImg = ["png", "jpg", "jpeg", "PNG", "JPG"];
            console.log(files)
                
            if(files['size'] > 1024*1024){
                errorSubmit.files = "Dung luong > 1mb";
                flag = false;
                
                }else{
                    let getFile = files['name'].split(".");
                
                if(!checkImg.includes(getFile[1])){
                    errorSubmit.files = "Sai dinh dang file";
                    flag = false;
                }
            }
        }
        

        if(!flag){
            this.setState({
                formErrors : errorSubmit
            })
        }else {
            
            const data = {
                name : this.state.name,
                email: this.state.email,
                password: this.state.password,
                phone: this.state.phone,
                address : this.state.address,
                country : this.state.country,
                avatar : this.state.avatar,
                level: this.state.level
            }
            axios.post("http://localhost/laravel/public/api/register", data)
            .then(res => {
               if(res.data.errors){
                   this.setState({
                    formErrors :  res.data.errors
                   })
                }
            })
            .catch(error => console.log(error))
        }
        
    }
        
    render() {
    
        return (
        <div className="App">
            <Errors formErrors={this.state.formErrors}/>
                <div className="col-sm-12">
                    <div className="signup-form">{/*sign up form*/}
                    <h2>New User Signup!</h2>
                    <div className="col-md-12">
                            <div className="card">
                            <div className="card-body">
                                <br />
                                <form encType="multipart/form-data" onSubmit={this.handleSubmit}>
                                {/* <input type="hidden" name="_token" defaultValue="6HgiBVK3bDtJTpvDwcmKHMLc31OYDObEe82lClDB" /> */}
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Full Name (*)</label>
                                    <div className="col-md-8">
                                    <input id="name" type="text" className="form-control " name="name"  onChange={this.handleInput} value={this.state.name}/>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Email (*)</label>
                                    <div className="col-md-8">
                                    <input id="name" type="text" className="form-control " name="email" onChange={this.handleInput} value={this.state.email}/>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Password (*)</label>
                                    <div className="col-md-8">
                                    <input id="name" type="password" className="form-control " name="password" onChange={this.handleInput} value={this.state.password}/>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Phone</label>
                                    <div className="col-md-8">
                                    <input id="phone" type="text" className="form-control " name="phone"  onChange={this.handleInput} value={this.state.phone}/>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Address</label>
                                    <div className="col-md-8">
                                    <input id="address" type="text" className="form-control " name="address" onChange={this.handleInput} value={this.state.address}/>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Avatar (*)</label>
                                    <div className="col-md-8">
                                    <input id="avatar" type="file" className="form-control " name="file" onChange={this.handleInputFile} multiple/>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Country (*)</label>
                                    <div className="col-md-8">
                                    <input id="country" type="text" className="form-control " name="country" onChange={this.handleInput} value={this.state.country}/>
                                    </div>
                                </div>
                                <div className="form-group row mb-0">
                                    <div className="col-md-8 offset-md-4">
                                    <button type="submit" className="btn btn-primary">
                                        Register
                                    </button>
                                    </div>
                                </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>{/*/sign up form*/}
                </div>
        </div>
        );
    }
}

export default Register;