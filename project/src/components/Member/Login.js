import axios from "axios"
import React , {Component} from "react"
import { withRouter } from "react-router-dom";

import Errors from "../../Errors";
import Register from "./Register";

import { AppContext } from "../../AppContext";
import App from "../../App";
class Login extends Component{
    static contextType = AppContext
    constructor(props){
        super(props)
        this.state ={
            email :"",
            password : "",
            formErrors : {},
            obj : {},
            flag : ""
        }
        this.handleInput=this.handleInput.bind(this)
        this.handleSubmit=this.handleSubmit.bind(this)
    }

    handleInput(e){
        const nameInput = e.target.name;
        const value = e.target.value;

        this.setState({
            [nameInput]: value
        })
    }

    handleSubmit(e){
        e.preventDefault();
        let email = this.state.email;
        let password = this.state.password;
        let errorSubmit = this.state.formErrors;

        let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        let flag = true ;

        errorSubmit.email = errorSubmit.password = ""
        if(email == ""){
            errorSubmit.email = "Vui long nhap email";
            flag = false;
        }else if( !emailReg.test(email) ) {
                flag = false;
                errorSubmit.email = "Vui long nhap dung dinh dang email ";
            }

        if(password == ""){
            errorSubmit.password = "Vui long nhap password";
            flag = false;
        }


        if(!flag){
            this.setState({
                formErrors : errorSubmit
            })
        }else{
            const data ={
                email : this.state.email,
                password : this.state.password,
                level : 0
            }
            axios.post("http://localhost/laravel/public/api/login",data)
            .then(res =>{
                if(res.data.errors){
                    this.setState({
                        formErrors: res.data.errors
                    })
                }else{
                    let abc = JSON.stringify(res.data);
                    localStorage.setItem("demo",abc)
                    this.context.stateLogin(true)
                    this.props.history.push('/')    
                }
            })
            .catch(error =>console.log(error))
        }
    }

    render(){
        return(
            <div className="col-sm-4 col-sm-offset-1">
                <Errors formErrors={this.state.formErrors}/>
                <div className="login-form" onSubmit={this.handleSubmit}>{/*login form*/}
                    <h2>Login to your account</h2>
                    <form action="" onSubmit={this.handlesubmit} enctype="multipart/form-data">
                        <input type="email" name="email"  placeholder="Email Address" onChange={this.handleInput} value={this.state.email}/>
                        <input type="password" name="password" placeholder="Password" onChange={this.handleInput} value={this.state.password}/>
                        <span>
                        <input type="checkbox" name="checkbox" className="checkbox" onChange={this.handleInput} /> 
                        Keep me signed in
                        </span>
                        <button type="submit" className="btn btn-default">Login</button>
                    </form>
                </div>{/*/login form*/}
                <div class="col-sm-1">
                    <h2 class="or">OR</h2>
                </div>
                <Register/>
            </div>    
            
        )
    }
}
export default withRouter(Login)