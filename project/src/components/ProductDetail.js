import { Component } from "react";
import axios from 'axios';
import { PopupboxManager, PopupboxContainer } from 'react-popupbox';
import "react-popupbox/dist/react-popupbox.css"

class ProductDetail extends Component{
    constructor(props){
        super(props)
        this.state={
            data:[],
            img: [],
            imageUp:""
        }
        this.renderData=this.renderData.bind(this)
        this.openPopupbox=this.openPopupbox.bind(this)
        this.renderImage=this.renderImage.bind(this)
        this.handleUp=this.handleUp.bind(this)
        this.renderSmallImg=this.renderSmallImg.bind(this)
    }
    componentDidMount() {
        axios.get("http://localhost/laravel/public/api/product/detail/"+this.props.match.params.id)
        .then(res => { 
            let image = JSON.parse(res.data.data.image) 
            this.setState({
                data: res.data.data,
                img: image,
                imageUp: image[0]
            })
        })
        .catch(error => console.log(error))
    }

    handleUp(e){
        this.setState({
            imageUp: e.target.name
        })
    }

    renderImage(){
        let {data, img}=this.state
        if(img) {
            return Object.keys(img).map((key,value)=>{
                return(
                    <a href><img src={"http://localhost/laravel/public/upload/user/product/" + data['id_user'] + "/small_" + img[key]} name={img[key]} alt=""  onClick={this.handleUp}/></a>
                )
            })
        }
        
    }
    renderSmallImg(){
        let item=[1,2,3]
        return item.map((item)=>{
            return(
                <div className={item==1 ? "item active" : "item"}>
                    {this.renderImage()}
                </div>
            )
        })
    }
    openPopupbox(){
        let {data}=this.state; 
        const content = <img src={"http://localhost/laravel/public/upload/user/product/" + data['id_user']  + "/" + this.state.imageUp } />
        PopupboxManager.open({
            content,
            config: {
            titleBar: {
                enable: true,
            },
            fadeIn: true,
            fadeInSpeed: 500
            }
        })
    }

    renderData(){
        let {data, img}=this.state; 
        console.log(this.state.imageUp)
        if(Object.keys(data).length > 0){
            return(
                <div className="product-details">{/*product-details*/}
                    <div className="col-sm-5">
                    <div className="view-product">
                        <img src={"http://localhost/laravel/public/upload/user/product/"+data['id_user']+"/larger_"+this.state.imageUp} alt="" />
                        <a href="#" rel="prettyPhoto" onClick={this.openPopupbox}><h3>ZOOM</h3></a>
                        {/* <button rel="prettyPhoto" onClick={this.openPopupbox}>ZOOM</button> */}
                        <PopupboxContainer />

                    </div>
                    <div id="similar-product" className="carousel slide" data-ride="carousel">
                        {/* Wrapper for slides */}
                        <div className="carousel-inner">
                            {this.renderSmallImg()}
                        {/* <div className="item active">
                            {this.renderImage()}
                        </div>
                        <div className="item">
                            {this.renderImage()}
                        </div>
                        <div className="item">
                            {this.renderImage()}
                        </div> */}
                        </div>
                        {/* Controls */}
                        <a className="left item-control" href="#similar-product" data-slide="prev">
                        <i className="fa fa-angle-left" />
                        </a>
                        <a className="right item-control" href="#similar-product" data-slide="next">
                        <i className="fa fa-angle-right" />
                        </a>
                    </div>
                    </div>
                    <div className="col-sm-7">
                    <div className="product-information">{/*/product-information*/}
                        <img src="images/product-details/new.jpg" className="newarrival" alt="" />
                        <h2>{data['name']}</h2>
                        <p>Web ID: 1089772</p>
                        <img src="images/product-details/rating.png" alt="" />
                        <span>
                        <span>US ${data['price']}</span>
                        <label>Quantity:</label>
                        <input type="text" defaultValue={3} />
                        <button type="button" className="btn btn-fefault cart">
                            <i className="fa fa-shopping-cart" />
                            Add to cart
                        </button>
                        </span>
                        <p><b>Availability:</b> In Stock</p>
                        <p><b>Condition:</b> New</p>
                        <p><b>Brand:</b>{data['brand']}</p>
                        <a href><img src="images/product-details/share.png" className="share img-responsive" alt="" /></a>
                    </div>{/*/product-information*/}
                    </div>
                </div>
            )
        }        
    }
    render(){
        return(
            <>
                {this.renderData()}
                <div className="category-tab shop-details-tab">{/*category-tab*/}
                    <div className="col-sm-12">
                    <ul className="nav nav-tabs">
                        <li><a href="#details" data-toggle="tab">Details</a></li>
                        <li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
                        <li><a href="#tag" data-toggle="tab">Tag</a></li>
                        <li className="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
                    </ul>
                    </div>
                    <div className="tab-content">
                    <div className="tab-pane fade" id="details">
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery1.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery2.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery3.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery4.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="companyprofile">
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery1.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery3.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery2.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery4.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="tag">
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery1.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery2.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery3.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-3">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/gallery4.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="tab-pane fade active in" id="reviews">
                        <div className="col-sm-12">
                        <ul>
                            <li><a href><i className="fa fa-user" />EUGEN</a></li>
                            <li><a href><i className="fa fa-clock-o" />12:41 PM</a></li>
                            <li><a href><i className="fa fa-calendar-o" />31 DEC 2014</a></li>
                        </ul>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <p><b>Write Your Review</b></p>
                        <form action="#">
                            <span>
                            <input type="text" placeholder="Your Name" />
                            <input type="email" placeholder="Email Address" />
                            </span>
                            <textarea name defaultValue={""} />
                            <b>Rating: </b> <img src="images/product-details/rating.png" alt="" />
                            <button type="button" className="btn btn-default pull-right">
                            Submit
                            </button>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="recommended_items">{/*recommended_items*/}
                <h2 className="title text-center">recommended items</h2>
                <div id="recommended-item-carousel" className="carousel slide" data-ride="carousel">
                    <div className="carousel-inner">
                    <div className="item active">	
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend1.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend2.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend3.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="item">	
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend1.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend2.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend3.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <a className="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                    <i className="fa fa-angle-left" />
                    </a>
                    <a className="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                    <i className="fa fa-angle-right" />
                    </a>			
                </div>
                
                    
                    
                </div>
            </>
        )
    }
}
export default ProductDetail;