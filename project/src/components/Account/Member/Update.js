import { jsxOpeningElement, thisExpression } from '@babel/types';
import axios from 'axios';
import React, {Component} from 'react';
import Errors from '../../../Errors';

class Update extends Component{
    constructor(props){
        super(props) 
        this.state = {
            id: '', 
            name: '',
            email: '',
            password:'',
            phone:'',
            address:'',
            country:'',
            avatar: '',
            files:'',
            msg:'',
            formErrors:{},
        }
        this.handleInput = this.handleInput.bind(this)
        this.handleInputFile = this.handleInputFile.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        
    }
    componentDidMount(){
        let obj= localStorage.getItem('demo')
        let demo=JSON.parse(obj)
        Object.keys(demo).map((key,index)=>{
            this.setState({
                id: demo[key]['id'],
                name: demo[key]['name'],
                email: demo[key]['email'],
                phone: demo[key]['phone'],
                address: demo[key]['address'],
                country: demo[key]['country'],
                //level: demo[key]['level']
            })
        })
    }

    handleInput(e){
        const nameInput = e.target.name;
        const value = e.target.value;

        this.setState({
            [nameInput]: value
        })
    }
    handleInputFile(e){
        const file = e.target.files;
        let reader = new FileReader()
        reader.onload = (e) => {
            this.setState({
            avatar: e.target.result,//gửi api qua
            files: file[0]
            })
        };
        reader.readAsDataURL(file[0])
        
    }
    
    handleSubmit(e){
        e.preventDefault();
        
        let flag = true
        let name = this.state.name;
        let email = this.state.email;
        let password = this.state.password;
        let phone = this.state.phone;
        let address = this.state.address;
        let country = this.state.country;
        let files = this.state.files;
        let level = this.state.level;
        let errorSubmit = this.state.formErrors;

        //let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        
        errorSubmit.phone = errorSubmit.name = errorSubmit.country = errorSubmit.address = errorSubmit.files  = ""

        if(name == ""){
            errorSubmit.name = "Vui long nhap name";
            flag = false;
        }

        if(phone == ""){
            errorSubmit.phone = "Vui long nhap phone";
            flag = false;
        }else if( !filter.test(phone) ) {
            flag = false;
            errorSubmit.phone = "Vui long nhap dung dinh dang phone ";
        }

        if(address == ""){
            errorSubmit.address = "Vui long nhap address";
            flag = false;
        }

        if(country == ""){
            errorSubmit.country = "Vui long nhap country";
            flag = false;
        }

        if(files == ""){
            errorSubmit.files = "Vui long chon file";
            flag = false;
        }else {
            let checkImg = ["png", "jpg", "jpeg", "PNG", "JPG"];
            console.log(files)
                
            if(files['size'] > 1024*1024){
                errorSubmit.files = "Dung luong > 1mb";
                flag = false;
                
                }else{
                    let getFile = files['name'].split(".");
                
                if(!checkImg.includes(getFile[1])){
                    errorSubmit.files = "Sai dinh dang file";
                    flag = false;
                }
            }
        }

        if(!flag){
            this.setState({
                formErrors : errorSubmit
            })
        }else {
            const formData = new FormData();
            formData.append("name", name);
            formData.append("email", email);
            formData.append("phone", phone);
            formData.append("password", password);
            formData.append("address", address);
            //formData.append("avatar", avatar);
            formData.append("country", country);
            //formData.append("level", level);

            let useData=JSON.parse(localStorage.getItem("demo"))
            let url = ("http://localhost/laravel/public/api/user/update/"+this.state.id)
            let accessToken = useData.success.token
            let config = {
                headers: {
                    "Authorization" : "Bearer " + accessToken,
                    "Content-type" : "application/x-www-form-urlencoded",
                    "Accept" : "application/json"
                }
            }
            axios.post(url ,formData , config)
            .then(res=>{
                if(res.data.errors){
                    this.setState({
                     formErrors :  res.data.errors
                    })
                }
                else{
                    console.log(res)
                    localStorage.setItem('demo',JSON.stringify(res.data))
                    this.setState({
                        msg :  "Update thành công"
                    })
                }
            })
            .catch(error => console.log(error))
       }
        
    }
    render(){
        return(
            <div className="col-sm-4 col-sm-offset-1">
                <div className="login-form" >{/*login form*/}
                    <h2> User Update !</h2>
                    <Errors formErrors={this.state.formErrors}/>
                    <p>{this.state.msg}</p>
                    <form encType="multipart/form-data" onSubmit={this.handleSubmit}>
                        <input type="text" name="name" onChange={this.handleInput} value={this.state.name}/>
                        <input type="email" readOnly onChange={this.handleInput} value={this.state.email}/>
                        <input type="password" name="password" onChange={this.handleInput} value={this.state.password}/>
                        <input type="text" name="address" onChange={this.handleInput} value={this.state.address}/>
                        <input type="text" name="country" onChange={this.handleInput} value={this.state.country}/>
                        <input type="text" name="phone" onChange={this.handleInput} value={this.state.phone}/>
                        <input id="avatar" type="file" className="form-control " name="file" onChange={this.handleInputFile} />
                        <button type="submit" className="btn btn-default">Signup</button>
                    </form>
                </div>
            </div>
        )
    }
}
export default Update;