import { Component } from "react";
import { Link } from "react-router-dom";

class MenuLeft extends Component {
    render(){
        return (
            <div className="col-sm-3">
                <div className="left-sidebar">
                    <h2>account</h2>
                    <div className="panel-group category-products" id="accordian">{/*category-productsr*/}
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                <Link data-toggle="collapse" data-parent="#accordian" to="/account/member">
                                    <span className="badge pull-right"><i className="fa fa-plus" /></span>
                                    Account
                                </Link>
                                </h4>
                            </div>
                        </div>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                <Link data-toggle="collapse" data-parent="#accordian" to="/account/myproduct">
                                    <span className="badge pull-right"><i className="fa fa-plus" /></span>
                                    My Product
                                </Link>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    
  }
export default MenuLeft;