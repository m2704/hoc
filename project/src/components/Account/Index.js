import { Component } from "react";
import App from "./App";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    
  } from "react-router-dom";
import Update from "./Member/Update";
import MyProduct from "./Product/MyProduct";
import AddEdit from "./Product/AddEdit";
import Edit from "./Product/Edit";
import Delete from "./Product/Delete";
class Index extends Component{
render(){
    return(
        <div>
            <App>
            <Switch>
                <Route path='/account/member' component={Update}/>
                <Route path='/account/myproduct' component={MyProduct}/>
                <Route exact path='/account/product/add' component={AddEdit}/>
                <Route exact path='/account/product/edit/:id' component={Edit}/>
                <Route exact path='/account/product/delete/:id' component={Delete}/>
            </Switch>
            </App>
        </div>
    )}
}
export default Index;