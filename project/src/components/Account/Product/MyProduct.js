import { Component } from "react";
import {
    Link
}from "react-router-dom"
import axios from 'axios';
class MyProduct extends Component{
    constructor(props){
        super(props)
        this.state={
          data:[],
        }
        this.renderPro=this.renderPro.bind(this)
    }
    componentDidMount(){
        let userData = JSON.parse(localStorage.getItem("demo"))
        let accessToken = userData.success.token
        let config ={
            headers:{
              'Authorization': 'Bearer '+ accessToken,
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json'
            }
          }
        let url ="http://localhost/laravel/public/api/user/my-product"
        axios.get(url,config)
        .then(res=>{
            this.setState({
                data:res.data.data,
            })
        })
        .catch(error=>console.log(error))
    }
    renderPro(){
        let {data}=this.state
        console.log(data)
        if(Object.keys(data).length>0){
            return Object.keys(data).map((key, value) => {                
                let image = JSON.parse(data[key]["image"]) 
                return(
                    <tr>
                        <td class="cart_id">
                            <p>{data[key]["id"]}</p>

                        </td>
                        <td class="cart_name">
                            <p>{data[key]["name"]}</p>
                        </td>
                        <td class="cart_image">
                            <a href=""><img style={{width: '70px'}} src={"http://localhost/laravel/public/upload/user/product/"+ data[key]["id_user"] +"/"+ image[0] } alt="" /></a>
                        </td>
                        <td class="cart_price">
                            <p>{data[key]["price"]}</p>
                        </td>
                        <td class="cart_action">
                        <Link class="cart_quantity_delete" to={"/account/product/edit/"+ data[key]['id']}><i class="fa fa-edit"></i></Link>
                        <Link class="cart_quantity_delete" to={"/account/product/delete/"+ data[key]['id']}><i class="fa fa-times"></i></Link>
                        </td>
                    </tr>

                )
            })
        }
    }
    render(){
        return(
            <div className="table-responsive col-md-9 cart_info">
                <table className="table table-condensed">
                    <thead>
                        <tr className="cart_menu" style={{width: '50px', color: '#fff', background: '#FE980F'}}>
                            <td className="Id">Id</td>
                            <td className="Name">Name</td>
                            <td className="image">Image</td>
                            <td className="Price">Price</td>
                            <td className="Action">Action</td>
                            <td />
                        </tr>
                    </thead>
                    <tbody>
						{this.renderPro()}
					</tbody>                    
                </table>
                <Link className="btn btn-default update" to="/account/product/add">Add New</Link>
            </div>
        )
        
    }
}
export default MyProduct;