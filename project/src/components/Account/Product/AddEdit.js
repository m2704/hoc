import { Component } from "react";
import axios from 'axios';
import Errors from "../../../Errors";
class AddEdit extends Component{
    constructor(props){
        super(props) 
        this.state = {
            category:"",
            brand: "",
            array_category:"",
            array_brand: "",
            name: "",
            files:[],
            avatar:"",
            price:"" ,
            detail:"",
            company:"",
            status:0,
            sale:"",
            msg:"",
            formErrors: {},
    
        }
        
        this.handleInput = this.handleInput.bind(this)
        this.handleInputFile = this.handleInputFile.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.renderBrand = this.renderBrand.bind(this)
        this.renderCategory = this.renderCategory.bind(this)
    }

    componentDidMount(){
        axios.get("http://localhost/laravel/public/api/category-brand")
        .then(res => { 
            this.setState({
                array_category : res.data.category,
                array_brand: res.data.brand,
            })
            
        })
        .catch(error => console.log(error))
    }
    

    handleInput(e){
        const nameInput = e.target.name;
        const value = e.target.value;

        this.setState({
            [nameInput]: value
        })
    }
    handleInputFile(e){
        //console.log(e.target.files)
        const files = e.target.files;
        this.setState({
            files: e.target.files
        })
        
    }

    readerSale(){
        if(this.state.status == 0){
            return(
                <input type="text" className="col-md-6" name="sale" placeholder="0" onChange={this.handleInput} value={this.state.sale}/>
            )
        }
    }

    renderCategory(){
        return(
        Object.keys(this.state.array_category).map((key, value)=>{
            return(
                <option value={this.state.array_category[key]['id']} >{this.state.array_category[key]['category']}</option>
            )
        })
        )
    }
    renderBrand(){
        return(
        Object.keys(this.state.array_brand).map((key, value)=>{
            return(
                <option value={this.state.array_brand[key]['id']} >{this.state.array_brand[key]['brand']}</option>
            )
        })
        )
    }

    handleSubmit(e){
        e.preventDefault();
        let userData = JSON.parse(localStorage.getItem("demo"))
        let flag = true
        let {name} = this.state
        let {category} = this.state
        let {brand} = this.state
        let {price} = this.state
        let {company} = this.state
        let {detail} = this.state
        let {sale} = this.state
        let {files} = this.state
        let {status} = this.state
        let errorSubmit = this.state.formErrors;

        errorSubmit.category  = errorSubmit.brand = errorSubmit.name = errorSubmit.files = errorSubmit.price= errorSubmit.sale = errorSubmit.condition = errorSubmit.detail = errorSubmit.company =""


        if(name == ""){
            errorSubmit.name = "Vui long nhap name";
            flag = false;
        }
        if(company == ""){
            errorSubmit.company = "Vui long nhap company";
            flag = false;
        }
        if(detail == ""){
            errorSubmit.detail = "Vui long nhap detail";
            flag = false;
        }
        if(price == ""){
            errorSubmit.price = "Vui long nhap price";
            flag = false;
        }

        if(category == ""){
            errorSubmit.category  = "Vui long chon category"
            flag = false;
         }
        
         if(brand == ""){
            errorSubmit.brand  = "Vui long chon brand"
            flag = false;
         }
        
        if(Object.keys(files).length == 0){
            errorSubmit.files = "Vui long chon file";
            flag = false;
        }else if(Object.keys(files).length > 3){
            errorSubmit.files  = "Toi da 3 hinh anh"
            flag = false;
         }else {
            let checkImg = ["png", "jpg", "jpeg", "PNG", "JPG"];
            Object.keys(files).map((item,value)=>{
                if(files[item]['size'] > 1024*1024){
                    errorSubmit.files = "Dung luong > 1mb";
                    flag = false;
                }else{
                    let getFile = files[item]['name'].split(".");
                
                if(!checkImg.includes(getFile[1])){
                    errorSubmit.files = "Sai dinh dang file";
                    flag = false;
                }
                }
            })
        }
        

        if(!flag){
            this.setState({
                formErrors : errorSubmit
            })
        }else {
            let formdata =  new FormData()
            formdata.append("name",name)
            formdata.append("price",price)
            formdata.append("category",category)
            formdata.append("brand",brand)
            formdata.append("company",company)
            formdata.append("detail",detail)
            formdata.append("status",status)
            formdata.append("sale",sale)
        
            Object.keys(files).map((item,value)=>{
                formdata.append("file[]", files[item])
            })
        
            let url = "http://localhost/laravel/public/api/user/add-product"
            let accessToken = userData.success.token
            let config ={
                headers:{
                  'Authorization': 'Bearer '+ accessToken,
                  'Content-Type': 'application/x-www-form-urlencoded',
                  'Accept': 'application/json'
                }
            }
        
            axios.post(url,formdata,config)
            .then(res=>{
                console.log(res)
                if(res.data.errors){
                    this.setState({
                        formErrors: res.data.errors
                    })
                }else{
                    this.setState({
                        msg: "Thêm thành công"
                    })
                }
                
            })
            .catch(error => console.log(error))
        }
        
    }

    render(){
        return(
            <div className="col-sm-6">
                <div className="product-form" >
                    <h2>Create product!</h2>
                    <Errors formErrors={this.state.formErrors}/>
                    <p>{this.state.msg}</p>
                    <form encType="multipart/form-data" onSubmit={this.handleSubmit}>
                        <input type="text" name="name" placeholder="Name" onChange={this.handleInput} value={this.state.name}/>
                        <br/>
                        <input type = "text" name="price" placeholder="Price" onChange={this.handleInput} value={this.state.price}/>
                        <select className="id_category" onChange={this.handleInput} name="category">
                            <option value="0" >Please choose category</option>
                            {this.renderCategory()}
                        </select>
                        <select className="id_brand" onChange={this.handleInput} name="brand">
                            <option value="0" >Please choose brand</option>
                            {this.renderBrand()}
                        </select>
                        <select className="id_status" onChange={this.handleInput} name="status">
                            <option >Please choose status</option>
                            <option value="0" >Sale</option>
                            <option value="1" >New</option>
                        </select>
                        {this.readerSale()}
                        
                        <br/>
                        <input type="text" name="company" placeholder="Company Profile" onChange={this.handleInput} value={this.state.company}/>
                        <br/>
                        <input id="avatar" type="file" className="form-control " name="files" onChange={this.handleInputFile} multiple/>
                        <br/>
                        <textarea type="text" name="detail" placeholder="Detail" onChange={this.handleInput} value={this.state.detail}/>
                        <button type="submit" className="btn btn-default">Signup</button>
                    </form>
                </div>
            </div>
        )
        
    }
}
export default AddEdit;