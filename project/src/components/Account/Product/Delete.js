import { Component } from "react";
import axios from 'axios';
import MyProduct from "./MyProduct";
import { Link } from "react-router-dom";

class Delete extends Component{
    constructor(props){
        super(props) 
        this.state = {
            data:[]
    
        }
        this.renderData=this.renderData.bind(this)
    }
    componentDidMount(){
        let userData = JSON.parse(localStorage.getItem("demo"))
        let accessToken = userData.success.token
        let config ={
            headers:{
              'Authorization': 'Bearer '+ accessToken,
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json'
            }
          }
        let url =("http://localhost/laravel/public/api/user/delete-product/"+this.props.match.params.id)
        axios.get(url,config)
        .then(res=>{
            this.setState({
                data: res.data.data
            })
            this.props.history.push("/account/myproduct")
        })
        .catch(error=>console.log(error))

        
    }
    renderData(){
        let {data}=this.state
        return(
            
            Object.keys(data).map((key, value)=>{
                if(data[key]){
                    delete data[key]
                }
            })
            
        )
    }
    render(){
        return(
            <>
            {this.renderData()}
            </>
        )
        
    }
}
export default Delete;