import { Component } from "react";
import axios from 'axios';
import { AppContext } from "../../AppContext";
class Cart extends Component{
    static contextType = AppContext
    constructor(props){
        super(props)
        this.state={
          data :{},
          qty:"",
        }
        this.renderData=this.renderData.bind(this)
        this.handleIncrease=this.handleIncrease.bind(this)
        this.handleDecrease=this.handleDecrease.bind(this)
        this.handleDelete=this.handleDelete.bind(this)
    }
    componentDidMount(){
        let cart = JSON.parse(localStorage.getItem("cart"))
        axios.post("http://localhost/laravel/public/api/product/cart", cart)
        
        .then(res =>{
            console.log(res)
            this.setState({
                data : res.data.data
            })
        })
        .catch(error =>console.log(error))
        // this.context.stateCartQty(Object.keys(cart).length)
    }



    handleIncrease(e){
        let {data}=this.state
        
        if(Object.keys(data).length>0){
            Object.keys(data).map((key, value) => {
                if(data[key]['id'] == e.target.id){
                    data[key]['qty'] ++
                }
                
            })
        }
        this.setState({
            data : data
        })
        let cart = JSON.parse(localStorage.getItem("cart"))
        if(Object.keys(cart).length>0){
            Object.keys(cart).map((key)=>{
                if(key == e.target.id){
                    cart[key]++
                }
            })
        }
        var def = JSON.stringify(cart);
        localStorage.setItem("cart",def)
    }
    handleDecrease(e){
        let {data}=this.state
        let cart = JSON.parse(localStorage.getItem("cart"))
        if(Object.keys(data).length>0){
            Object.keys(data).map((key, value) => {
                if(data[key]['id'] == e.target.id){
                    if(data[key]['qty']>1){
                        data[key]['qty']--
                        
                    }else{
                        delete data[key]
                    }
                    
                    
                }
            })
        }
        this.setState({
            data : data
        })

        if(Object.keys(cart).length>0){
            Object.keys(cart).map((key)=>{
                if(key == e.target.id){
                    if(cart[key]>1){
                        cart[key]--
                    }else{
                        delete cart[key]
                    }
                    
                }
            })
        }
        var def = JSON.stringify(cart);
        localStorage.setItem("cart",def)
    }
    handleDelete(e){
        let {data}=this.state
        
        if(Object.keys(data).length>0){
            Object.keys(data).map((key, value) => {
                if(data[key]['id'] == e.target.id){
                    delete data[key]            
                }
            })
        }
        this.setState({
            data : data
        })

        let cart = JSON.parse(localStorage.getItem("cart"))
        if(Object.keys(cart).length > 0){
            Object.keys(cart).map((key)=>{
                if(key == e.target.id){
                    delete cart[key]
                }
            })
        }
        
        var def = JSON.stringify(cart);
        localStorage.setItem("cart",def)
        this.context.stateCartQty(Object.keys(cart).length)
    }

    getTotal(){
        
        let {data}=this.state
        //console.log(Object.keys(data).length)
        let sub_total = 0
        let total = 0
        Object.keys(data).map((key, value) => {
            sub_total = data[key]['price'] * data[key]['qty']
            total += sub_total
        })
        return total

    }

    renderData(){
        let {data}=this.state
        
        if(Object.keys(data).length>0){
            return Object.keys(data).map((key, value) => {
                let qty = data[key]['qty']
                let sub_total = data[key]['price'] * data[key]['qty']
                let image = JSON.parse(data[key]["image"]) 
                return(
                <tr>
                    <td className="cart_product">
                        <a href=""><img src={"http://localhost/laravel/public/upload/user/product/"+ data[key]["id_user"] +"/"+ image[0] } alt="" /></a>
                    </td>
                    <td className="cart_description">
                        <h4><a href="">{data[key]['name']}</a></h4>
                        <p>Web ID: 1089772</p>
                    </td>
                    <td className="cart_price">
                        <p>${data[key]['price']}</p>
                    </td>
                    <td className="cart_quantity">
                        <div clclassNamess="cart_quantity_button">
                            <a className="cart_quantity_up" id={data[key]['id']}  onClick={this.handleIncrease}> + </a>
                            <input className="cart_quantity_input" type="text" name="quantity" value={qty} defaultValue={1} autoComplete="off" size={2} />                                    
                            <a className="cart_quantity_down"  id={data[key]['id']} onClick={this.handleDecrease}> - </a>
                        </div>
                    </td>
                    <td className="cart_total">
                        <p className="cart_total_price">${sub_total}</p>
                    </td>
                    <td className="cart_delete">
                        <a className="cart_quantity_delete" id={data[key]['id']} onClick={this.handleDelete}><i className="fa fa-times"></i></a>
                    </td>
                </tr>

                )
                
            })
        }
    }
    render(){
        return(
            <>
            <div className="container">
                <div className="breadcrumbs">
                    <ol className="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li className="active">Shopping Cart</li>
                    </ol>
                </div>
                <div className="table-responsive cart_info">
                    <table className="table table-condensed">
                        <thead>
                            <tr className="cart_menu">
                                <td className="image">Item</td>
                                <td className="description" />
                                <td className="price">Price</td>
                                <td className="quantity">Quantity</td>
                                <td className="total">Total</td>
                                <td />
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderData()}
                        
                        </tbody>
                    </table>
                </div>
            </div>
            <section id="do_action">
                <div className="container">
                    <div className="heading">
                    <h3>What would you like to do next?</h3>
                    <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                    </div>
                    <div className="row">
                    <div className="col-sm-6">
                        <div className="chose_area">
                        <ul className="user_option">
                            <li>
                            <input type="checkbox" />
                            <label>Use Coupon Code</label>
                            </li>
                            <li>
                            <input type="checkbox" />
                            <label>Use Gift Voucher</label>
                            </li>
                            <li>
                            <input type="checkbox" />
                            <label>Estimate Shipping &amp; Taxes</label>
                            </li>
                        </ul>
                        <ul className="user_info">
                            <li className="single_field">
                            <label>Country:</label>
                            <select>
                                <option>United States</option>
                                <option>Bangladesh</option>
                                <option>UK</option>
                                <option>India</option>
                                <option>Pakistan</option>
                                <option>Ucrane</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                            </li>
                            <li className="single_field">
                            <label>Region / State:</label>
                            <select>
                                <option>Select</option>
                                <option>Dhaka</option>
                                <option>London</option>
                                <option>Dillih</option>
                                <option>Lahore</option>
                                <option>Alaska</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                            </li>
                            <li className="single_field zip-field">
                            <label>Zip Code:</label>
                            <input type="text" />
                            </li>
                        </ul>
                        <a className="btn btn-default update" href>Get Quotes</a>
                        <a className="btn btn-default check_out" href>Continue</a>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="total_area">
                        <ul>
                            <li>Cart Sub Total <span className="sub-total">${this.getTotal()}</span></li>
                            <li>Eco Tax <span>$2</span></li>
                            <li>Shipping Cost <span>Free</span></li>
                            <li>Total <span className="total2">${this.getTotal()}</span></li>
                        </ul>
                        <a className="btn btn-default update" href>Update</a>
                        <a className="btn btn-default check_out" href>Check Out</a>
                        </div>
                    </div>
                </div>
            </div>
            </section>
            
            </>
        )
        
    }

}export default Cart;


