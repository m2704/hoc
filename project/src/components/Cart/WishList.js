import { Component } from "react";
import axios from 'axios';
import { AppContext } from "../../AppContext";
class Wishlist extends Component{
    static contextType = AppContext
    constructor(props){
        super(props)
        this.state={
          data : {},
          wishlist : {},
          qty : "",
          cart : {}
        }
        this.renderData=this.renderData.bind(this)
        this.handleDelete=this.handleDelete.bind(this)
        this.handelAddCart=this.handelAddCart.bind(this)
    }
    componentDidMount(){
        axios.get("http://localhost/laravel/public/api/product/wishlist")
        .then(res =>{
            this.setState({
                data : res.data.data,
                wishlist : JSON.parse(localStorage.getItem("wishlist"))
            })
        })
        .catch(error =>console.log(error))
    }

    handelAddCart(e){
        let id = e.target.id
        let qty=1
        let {cart}=this.state  
        let getCart = localStorage.getItem("cart")
        if(getCart){
            cart =JSON.parse(getCart)      
        }
        if(cart){
            Object.keys(cart).map((key,index)=>{
                if(key == id){
                    qty = cart[key] + 1
                }
            })
        }
        cart[id] = qty
        this.context.stateCartQty(Object.keys(cart).length)
        let abc = JSON.stringify(cart);
        localStorage.setItem("cart",abc)
    }

    handleDelete(e){
        let {wishlist} =this.state
       
        wishlist.map((value, key)=> {
            if(e.target.id == value) {
                wishlist.splice(key,1)
            }
        })
        console.log(wishlist)
                   
                    
               
       
        this.setState({
            wishlist: wishlist
        })
        var def = JSON.stringify(wishlist);
        localStorage.setItem("wishlist",def)
        this.context.stateWhistList(Object.keys(wishlist).length)

        
    }
    renderData(){
        let {data,wishlist}=this.state
        if(Object.keys(wishlist).length>0){
            if(Object.keys(data).length>0){
                let filterArray = data.filter(item => wishlist.includes(item.id.toString()))
                return filterArray.map((item,i) => {
                    let image = JSON.parse(item.image) 
                    return(
                        <tr>
                            <td className="cart_product">
                                <a href=""><img src={"http://localhost/laravel/public/upload/user/product/"+ item.id_user +"/"+ image[0] } alt="" /></a>
                            </td>
                            <td className="cart_description">
                                <h4><a href="">{item.name}</a></h4>
                                <p>Web ID: 1089772</p>
                            </td>
                            <td className="cart_price">
                                <p>${item.price}</p>
                            </td>
                            <td>
                                <a  className="btn btn-default add-to-cart" id={item.id} onClick={this.handelAddCart}><i className="fa fa-shopping-cart" />Add to cart</a>
                            </td>
                            <td className="cart_delete">
                                
                                <a className="cart_quantity_delete" id={item.id} onClick={this.handleDelete}><i className="fa fa-times"></i></a>
                            </td>
                        </tr>

                    )
                
            }) 
            }
            
        }
    }
    render(){
        return(
            <>
            <div className="container">
                <div className="breadcrumbs">
                    <ol className="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li className="active">WishList</li>
                    </ol>
                </div>
                <div className="table-responsive cart_info">
                    <table className="table table-condensed">
                        <thead>
                            <tr className="cart_menu">
                                <td className="image">Item</td>
                                <td className="description" />
                                <td className="price">Price</td>
                                <td />
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderData()}
                        
                        </tbody>
                    </table>
                </div>
            </div>
            
            </>
        )
        
    }

}export default Wishlist;


