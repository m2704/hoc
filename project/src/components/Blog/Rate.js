import axios from "axios";
import React,{Component} from "react";
import StarRatings from "react-star-ratings";
import Errors from "../../Errors";

class Rate extends Component{
    constructor(props){
      super(props)
      this.state = {
          data: [],
          formErrors : "",
          rating : 0
          
      }
      this.changeRating = this.changeRating.bind(this)
    };

    componentDidMount() {
        axios.get("http://localhost/laravel/public/api/blog/rate/" + this.props.idBlog)
          .then(res => { 
                let data = res.data.data  
                let sl = Object.keys(data).length
                let tong = 0 
                let average=0
                if(Object.keys(data).length > 0){
                    Object.values(data).map((key,i)=>{ 
                        tong = parseInt(tong) + parseInt(key.rate)
                    })
                    average = parseFloat(tong/sl)
                    this.setState({
                        rating: average,
                        sl: sl
                    })
                }
          })
          .catch(error => console.log(error))
    }
    
    changeRating( newRating, name ) {
        let userData = JSON.parse(localStorage.getItem("demo"))
        let errorSubmit = this.state.formErrors
        let rating = this.state.rating
        let flag=true     
        this.setState({
            rating: newRating
        });
        if(userData == null){
            flag = false                                  
            errorSubmit.rating = "Vui lòng đăng nhập"
        }

        if(!flag){
            this.setState({
                formErrors: errorSubmit
            });
        }else{
            let url = "http://localhost/laravel/public/api/blog/rate/" + this.props.idBlog
            let accessToken = userData.success.token
            let config={
                headers:{
                    "Authorization": "Bearer "+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'                   
                }
            }
        
            if(rating){
                const formData = new FormData();    
                    formData.append("blog_id",this.props.idBlog)
                    formData.append("user_id",userData.Auth.id)
                    formData.append("rate",this.state.rating)

                axios.post(url,formData,config)
                .then(res =>{  
                    this.setState({
                        data : res.data,
                        formErrors : res.data.message
                    })
                    console.log(res.data)
                })
                .catch(error => console.log(error))
            } 
        }
    }
    render(){
        return (
            <>
            <div className="rating-area">
            <p>
                {this.state.formErrors}
            </p>
              <ul className="ratings">
                <li className="rate-this">Rate this item:</li>
                <li> 
                    <StarRatings
                        starDimension="40px"
                        starSpacing="15px" 

                        rating={this.state.rating}
                        starRatedColor="#FE980F"
                        changeRating={this.changeRating}
                        numberOfStars={5}
                        name='rating'
                    />
                </li>
                <li className="color">({this.state.sl} votes)</li>
              </ul>
              <ul className="tag">
                    <li>TAG:</li>
                    <li><a className="color" href>Pink <span>/</span></a></li>
                    <li><a className="color" href>T-Shirt <span>/</span></a></li>
                    <li><a className="color" href>Girls</a></li>
              </ul>
            </div>
            <div className="socials-share">
                <a href><img src="http://localhost/laravel/public/frontend/images/blog/socials.png" alt="" /></a>
            </div>
            </>
          );
    }
}
export default Rate