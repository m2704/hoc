import { Component } from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
class Index extends Component {
  constructor(props){
    super(props)
    this.state={
      data :[]
    }
  }
  componentDidMount() {
    axios.get("http://localhost/laravel/public/api/blog")
      .then(res => {
        this.setState({
          data : res.data.blog.data 
        })
      })
      .catch(error => console.log(error))
  }
  renderData(){
    let {data}=this.state;
    if(data.length > 0){
      return data.map((value, key) => {
        return(
          <div className="single-blog-post">
            <h3>{value['title']}</h3>
            <div className="post-meta">
              <ul>
                <li><i className="fa fa-user" /> Mac Doe</li>
                <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
              </ul>
              <span>
                <i className="fa fa-star" />
                <i className="fa fa-star" />
                <i className="fa fa-star" />
                <i className="fa fa-star" />
                <i className="fa fa-star-half-o" />
              </span>
            </div>
            <a href>
              <img src={"http://localhost/laravel/public/upload/Blog/image/" + value['image']} alt="" />
            </a>
            <p>{value['description']}</p>
            <Link to={"/blog/detail/"+ value['id']} className="btn btn-primary">Read more</Link>
          </div>
        );
      })
    }
  }
    render(){
        return(
          <div className="col-sm-9">
            <div className="blog-post-area">
              <h2 className="title text-center">Latest From our Blog</h2>
              {this.renderData()}
            </div>
          </div>
        )
    }
    
  }
export default Index;
