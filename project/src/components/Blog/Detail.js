import { Component } from "react";
import axios from 'axios';
import Comment from "./Comment";
import ListComment from "./ListComment";
import Rate from "./Rate";
class Detail extends Component {
  constructor(props){
    super(props)
    this.state={
      data :{},
      comment:"",
      listCmt:{},
      idBlog: this.props.match.params.id
    }
    this.getIdReplay=this.getIdReplay.bind(this)
    this.getData=this.getData.bind(this)
    this.renderData=this.renderData.bind(this)
  }
  componentDidMount() {
    axios.get("http://localhost/laravel/public/api/blog/detail/" + this.props.match.params.id)
      .then(res => { 
        console.log(res.data.data)
        this.setState({
          data : res.data.data,
          listCmt :res.data.data.comment,
          id : res.data.data.comment.id
                
                
        })
            
      })
      .catch(error => console.log(error))
  }
  getData(data){
    this.setState({
      listCmt: this.state.listCmt.concat(data)
    })
  }
  getIdReplay(id){
    console.log(id)
    this.setState({
      idRep: id
    })
  }
  renderData(){
    let {data}=this.state;  
    if(Object.keys(data).length > 0){
        return(
            <div className="single-blog-post">
                <h3>{data['title']}</h3>
                <div className="post-meta">
                <ul>
                    <li><i className="fa fa-user" /> Mac Doe</li>
                    <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                    <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                </ul>
                {/* <span>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                </span> */}
                </div>
                <a href>
                <img src={"http://localhost/laravel/public/upload/Blog/image/" + data['image']} alt="" />
                </a>
                <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p> <br />
                <p>
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p> <br />
                <p>
                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p> <br />
                <p>
                Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                </p>
                <div className="pager-area">
                <ul className="pager pull-right">
                    <li><a href="#">Pre</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
                </div>
            </div>
        ) 
    }
  }
    render(){
        return(
          <div className="col-sm-9">
            <div className="blog-post-area">
              <h2 className="title text-center">Latest From our Blog</h2>
              {this.renderData()}
              <Rate idBlog={this.state.idBlog}/>
              <div class="response-area">
                  <h2>3 RESPONSES</h2>
                  <ListComment comment={this.state.listCmt} getIdReplay={this.getIdReplay}/>
              </div>
              <Comment idBlog={this.state.idBlog} idRep={this.state.idRep} getData={this.getData} />
            </div>
          </div>
        )
    }
    
  }
export default Detail;
