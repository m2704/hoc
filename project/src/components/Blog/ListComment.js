import React ,{Component} from "react";

class ListComment extends Component{
  constructor(props){
    super(props)
    this.renderData = this.renderData.bind(this)
    this.getID = this.getID.bind(this)
  }
  getID(e){
    this.props.getIdReplay(e.target.id)
  }

  renderData(){
    const {comment}=this.props
    if(comment.length >0){
      return comment.map((value, key) => {
        if(value.id_comment==0){
          return(
            <div >
              <li className="media">
                <a className="pull-left" href="#"  >
                  <img className="media-object" style={{width: '121px', height: '86px'}} alt="" src={"http://localhost/laravel/public/upload/user/avatar/" + value.image_user}/>
                </a>
                <div className="media-body">
                  <ul className="sinlge-post-meta">
                    <li><i className="fa fa-user" />{value.name_user}</li>
                    <li><i className="fa fa-clock-o" />{value.created_at}</li>
                    <li><i className="fa fa-calendar" />{value.updated_at}</li>
                  </ul>
                  <p>{value.comment} </p>
                  <a className="btn btn-primary" id={value.id} onClick= {this.getID} ><i className="fa fa-reply" />Replay</a>
                </div>
              </li>
               
              {comment.map((value2,j) => {
                if(value.id==value2.id_comment){
                  return(
                    <li className="media second-media">
                      <a className="pull-left" href="#">
                        <img className="media-object" style={{width: '121px', height: '86px', marginLeft:'10px'}} src={"http://localhost/laravel/public/upload/user/avatar/" + value2.image_user} alt="" />
                      </a>
                      <div className="media-body">
                        <ul className="sinlge-post-meta">
                          <li><i className="fa fa-user" />{value.name_user}</li>
                          <li><i className="fa fa-clock-o" /> {value.created_at}</li>
                          <li><i className="fa fa-calendar" />{value.updated_at}</li>
                        </ul>
                        <p>{value2.comment}</p>
                        <a className="btn btn-primary"><i className="fa fa-reply" />Replay</a>
                      </div>
                    </li>
                  )
                }
              })}
            </div>
          )     
        }
        
      })
    }
  }


    render(){
        return(
            <ul class="media-list">
              {this.renderData()}
            </ul>
            
        )
    }
}

export default ListComment