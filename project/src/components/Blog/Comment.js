import { Component } from "react";
import axios from 'axios';
import Errors from "../../Errors";
class Comment extends Component {
    constructor(props){
        super(props)
        this.state={
          idComment: 0,
          comment:"",
          formErrors:{},
          data :{},
          getCmt:{}
        }
        this.postComment = this.postComment.bind(this)
        this.handleInput = this.handleInput.bind(this)
      }
      handleInput(e){
        const nameInput = e.target.name;
        const value = e.target.value;
    
        this.setState({
            [nameInput]: value
        })
      }
      
      postComment(e){
        e.preventDefault()

        let userData=JSON.parse(localStorage.getItem("demo"))
        let flag=true;
        let comment = this.state.comment
        let errorSubmit = this.state.formErrors;

        errorSubmit.comment=""
        
        console.log(this.props)

        if(userData == null){
          errorSubmit.comment="Vui lòng đăng nhập";
          flag=false;
        }else if(comment == ""){
          errorSubmit.comment="Bạn chưa nhập Comment";
          flag=false;
        }

        if(!flag){
          this.setState({
              formErrors : errorSubmit
          })
        }else{
          
          let url = ("http://localhost/laravel/public/api/blog/comment/" + this.props.idBlog)
          let accessToken = userData.success.token
          let config = {
            headers: {
                  "Authorization" : "Bearer " + accessToken,
                  "Content-type" : "application/x-www-form-urlencoded",
                  "Accept" : "application/json"
              }
          }
          if(comment){
              const formData = new FormData();
              formData.append("id_blog", this.props.idBlog);
              formData.append("id_user", userData.Auth.id);
              formData.append("id_comment", this.props.idRep ? this.props.idRep :0);
              formData.append("comment",this.state.comment);
              formData.append("image_user",userData.Auth.avatar)
              formData.append("name_user",userData.Auth.name) 
              
      
            axios.post(url ,formData , config)
            .then(res=>{
              console.log(res.data.data)
              this.setState({
                data : res.data,
                //idComment: this.props.idRep
              })
              this.props.getData(res.data.data)
              
            })
            .catch(error => console.log(error))
          }
        }
      }
        render(){
          console.log(this.props.idRep)
            return(
              <>
                <div class="replay-box">
                  <div class="row">
                    <div class="col-sm-12">
                      <h2>Leave a replay</h2>
                      <Errors formErrors={this.state.formErrors}/>
                      <div class="text-area">
                        <div class="blank-arrow">
                          <label>Your Name</label>
                        </div>
                        <span>*</span>
                        <textarea id="cmt" name="comment" rows="11" onChange={this.handleInput} value={this.state.comment} ></textarea>
                        <a onClick={this.postComment} class="btn btn-primary" >post comment</a>
                      </div>
                    </div>
                  </div>
                </div>
            </>
            )
        }
}
export default Comment;