import logo from './logo.svg';
import './App.css';
import {Component} from 'react';
import { withRouter } from 'react-router-dom';
import Head from './components/Layout/Head';
import Footer from './components/Layout/Footer';
import MenuLeft from './components/Layout/MenuLeft';
import { AppContext } from './AppContext';
import Slide from './components/Layout/Slide';
import Login from './components/Member/Login';
class App extends Component{
    constructor(props){
        super(props)
        this.state={
            flag: false,
            cart: ""

        }
        this.stateLogin=this.stateLogin.bind(this)
        this.stateCartQty=this.stateCartQty.bind(this)
        this.stateWhistList=this.stateWhistList.bind(this)
    }
    stateLogin(data){
        let abc = JSON.stringify(data);
        localStorage.setItem("login",abc)
    }
    stateCartQty(qty){
        this.setState({
            cart : qty
        })
        let stateCart = JSON.stringify(qty);
        localStorage.setItem("qty_cart",stateCart)
    }
    stateWhistList(data){
        console.log(data)
        this.setState({
            wishList : data
        })
        let stateWhistList = JSON.stringify(data);
        localStorage.setItem("wishlist_qty",stateWhistList)
    }
    render(){
        let path= this.props.location.pathname
        return(
                <>
                    <AppContext.Provider value={{
                        state: this.state,
                        stateLogin: this.stateLogin,
                        stateCartQty: this.stateCartQty,
                        stateWhistList: this.stateWhistList
                    }}>
                        <Head />
                        <section>
                            <div className="container">
                                <div className="row">
                                    {/* <MenuLeft/> */}
                                    {path.includes("account") || path.includes("Cart") || path.includes("wishlist") ? "" : <MenuLeft/>}
                                    {this.props.children }
                                </div>
                            </div>
                        </section>
                        <Footer/>
                    </AppContext.Provider>
                    
                </>
        )
        
    }

}
export default withRouter(App);
