import { Component } from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
import { AppContext } from "./AppContext";
class Home extends Component {
    static contextType = AppContext
    constructor(props){
        super(props)
        this.state={
            data:[],
            cart: {},
            wishlist:{},
            sub_wishlist:[]
        }
        this.renderData=this.renderData.bind(this)
        this.handelAddCart=this.handelAddCart.bind(this)
        this.handelAddWhishList=this.handelAddWhishList.bind(this)
    }

    componentDidMount() {
        axios.get("http://localhost/laravel/public/api/product")
        .then(res => { 
            this.setState({
                data: res.data.data
            })
        })
        .catch(error => console.log(error))
    }
    handelAddCart(e){
        let id = e.target.name
        let qty=1
        let {cart}=this.state   
        let getCart = localStorage.getItem("cart")
        if(getCart){
            cart =JSON.parse(getCart)      
        }     
        if(cart){
            Object.keys(cart).map((key,index)=>{
                if(key == id){
                    qty = cart[key]+1
                }
            })
        }
        cart[id] = qty
        this.context.stateCartQty(Object.keys(cart).length)
        let abc = JSON.stringify(cart);
        localStorage.setItem("cart",abc)
    }
    handelAddWhishList(e){
        let id = e.target.id
        let {sub_wishlist} = this.state  
        let wishlist = localStorage.getItem("wishlist")
        if(wishlist){
            sub_wishlist =JSON.parse(wishlist)      
        }
        if(!sub_wishlist.includes(id)){
            sub_wishlist.push(id)
        }
        
        this.context.stateWhistList(Object.keys(sub_wishlist).length)
        localStorage.setItem("wishlist",JSON.stringify(sub_wishlist))
    }

    renderData(){
        let {data}=this.state;  
        if(Object.keys(data).length > 0){
            return Object.keys(data).map((key, value)=>{
                let image = JSON.parse(data[key]["image"]) 
                return(
                        <div className="col-sm-4">
                            <div className="product-image-wrapper">
                                <div className="single-products">
                                <div className="productinfo text-center">
                                    <img id="pd1" src={"http://localhost/laravel/public/upload/user/product/"+data[key]['id_user']+"/"+image[0]} alt="" />
                                    <h2>${data[key]['price']}</h2>
                                    <p>{data[key]['name']}</p>
                                    <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                </div>
                                <div className="product-overlay">
                                    <div className="overlay-content">
                                    <h2>${data[key]['price']}</h2>
                                    <p>{data[key]['name']}</p>
                                    <a className="btn btn-default add-to-cart" name={data[key]["id"]} onClick={this.handelAddCart}><i className="fa fa-shopping-cart"  />Add to cart</a>
                                    <br/>
                                    <Link to={"/product/detail/"+data[key]['id']} className="btn btn-default add-to-cart">More</Link>
                                    </div>
                                </div>
                                </div>
                                <div className="choose">
                                <ul className="nav nav-pills nav-justified">
                                    <li><a id={data[key]["id"]} onClick={this.handelAddWhishList}><i className="fa fa-plus-square" />Add to wishlist</a></li>
                                    <li><a href="#"><i className="fa fa-plus-square" />Add to compare</a></li>
                                </ul>
                                </div>
                            </div>
                        </div>
                    
                )
            })
            
            
            
        }
        
    }
    
    render(){
        return (
            <>
                <section>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-9 padding-right">
                                <div className="features_items">{/*features_items*/}
                                    <h2 className="title text-center">Features Items</h2>
                                    {this.renderData()}
                                </div>{/*features_items*/}
                                
                            </div>
                        </div>
                    </div>
                    
                </section>
            </>
        );
    }
  }
export default Home;
