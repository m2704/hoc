import React from 'react';
import { Component } from 'react';
import axios from 'axios';

class PersonList extends Component {
  constructor(props){
    super(props)
    this.state = {
      data:[]
    }
  }
  componentDidMount(){
    axios.get('https://jsonplaceholder.typicode.com/users')
    .then((res) => {
      this.setState({
        data: res.data
      })
    })
    .catch(error => console.log(error))

  }
  renderData(){
    let {data}=this.state;
    if(data.length > 0){
      return data.map((value,key)=>{
        return(
          <li key={key}>{value["id"]}</li>
        )
      })
    }
  }

  render() {
    console.log(this.state.data)
    return (
      <ul>
        {this.renderData()}
      </ul>
    )
  }
}
export default PersonList;