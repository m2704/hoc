import logo from './logo.svg';
import './App.css';
import Register from './Register';
import Login from './Login';
import PersonList from './PersonList';

function App() {
  return (
    <div className="App">
      {/* <Register/> */}
      {/* <Login/> */}
      <PersonList/>
    </div>
  );
}

export default App;
