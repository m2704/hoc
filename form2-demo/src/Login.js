import React, {Component} from 'react';
import Errors from './Errors';
class Login extends Component {
  constructor(props){
    super(props) 
    this.state = {
        email: '',
        pass:'',
        formErrors: {},
  
    }
    this.state2={}
      this.handleInput = this.handleInput.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInput(e){
        const nameInput = e.target.name;
        const value = e.target.value;

        this.setState({
            [nameInput]: value
        })
    }
    handleSubmit(e){
        e.preventDefault();

        let flag = true
        let email = this.state.email;
        let pass = this.state.pass;
        let errorSubmit = this.state.formErrors;
        let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        errorSubmit.email = errorSubmit.pass = errorSubmit.login= ""
        

        if(!email){
            flag = false;
            errorSubmit.email = "Vui long nhap email";
        }else if( !emailReg.test(email) ) {
                flag = false;
                errorSubmit.email = "Vui long nhap dung dinh dang email ";
            }

        if(!pass){
            flag = false;
            errorSubmit.pass = "Vui long nhap password";
        }

        
        this.state2= localStorage.getItem('demo')
        if(this.state){
            var demo = JSON.parse(this.state2)
            let email1=demo.email;
            let pass1= demo.pass; 
            if(flag == true)
            if(email!=email1 || pass!=pass1){
                flag = false;
                errorSubmit.login = "email hoac pass khong dung";
            }
        }

        if(!flag){
            this.setState({
                formErrors : errorSubmit
            })
        }else {
            this.setState({
                formErrors : {}
            })
            alert("Dang nhap thanh cong")
        }

        

    }
    render() {
    
        return (
        <div className="App">
            <Errors formErrors={this.state.formErrors}/>
            <form onSubmit={this.handleSubmit} >
                    <input type="text" placeholder="Email" name="email" onChange={this.handleInput}/>
                    <br/>
                    <input type="password" name="pass" onChange={this.handleInput}/>
                    <br/>
                    <button type="submit" className="btn btn-default">Login</button>
            </form>
            
        </div>
        );
    }
}

export default Login;