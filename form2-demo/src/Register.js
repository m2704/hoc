import React, {Component} from 'react';
import Errors from './Errors';
class Register extends Component {
  constructor(props){
    super(props) 
    this.state = {
      email: '',
      pass:'',
      img : '',
      formErrors: {},

    }
    
    this.handleInput = this.handleInput.bind(this)
    this.handleInputFile = this.handleInputFile.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    
    }
    handleInput(e){
        const nameInput = e.target.name;
        const value = e.target.value;

        this.setState({
            [nameInput]: value
        })
    }
    handleInputFile(e){
        let file = e.target.files
        this.setState({
        img: file
        })
    }
    handleSubmit(e){
        e.preventDefault();

        
        
        let obj={}
        
        let flag = true
        let email = this.state.email;
        let pass = this.state.pass;
        let img = this.state.img;
        let errorSubmit = this.state.formErrors;

        let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        
        errorSubmit.email = errorSubmit.pass = errorSubmit.img = ""
        if(email == ""){
            errorSubmit.email = "Vui long nhap email";
            flag = false;
        }else if( !emailReg.test(email) ) {
                flag = false;
                errorSubmit.email = "Vui long nhap dung dinh dang email ";
            }

        if(pass == ""){
            errorSubmit.pass = "Vui long nhap password";
            flag = false;
        }

        if(img == ""){
            errorSubmit.img = "Vui long chon file";
            flag = false;
        }else {
            let checkImg = ["png", "jpg", "jpeg", "PNG", "JPG"];
            Object.keys(img).map((key,index) => {
                if(img[key]['size'] > 1024*1024){
                    errorSubmit.img = "Dung luong > 1mb";
                    flag = false;
                    }else{
                        let getFile = img[key]['name'].split(".");
                    
                    if(!checkImg.includes(getFile[1])){
                        errorSubmit.img = "Sai dinh dang file";
                        flag = false;
                    }
                }
            })
        }
        

        if(!flag){
            this.setState({
                formErrors : errorSubmit
            })
        }else {
            this.setState({
                formErrors : {}
            })
            alert("ok")
            obj = {"email": email, "pass" : pass, "img": img}
            let abc = JSON.stringify(obj);
                localStorage.setItem("demo",abc)
        }
        
            
        
    }
        
    render() {
    
        return (
        <div className="App">
            <Errors formErrors={this.state.formErrors}/>
            <form onSubmit={this.handleSubmit} >
                    <input type="text" placeholder="Email" name="email" onChange={this.handleInput}/>
                    <br/>
                    <input type="password" name="pass" onChange={this.handleInput}/>
                    <br/>
                    <input type="file" name="img" onChange={this.handleInputFile} multiple/> 
                    <br/>
                    <button type="submit" className="btn btn-default">Register</button>
            </form>
            
        </div>
        );
    }
}

export default Register;